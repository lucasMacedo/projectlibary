import express from "express";
import routes from './routes';
import bodyParser from 'body-parser';
import cors from 'cors';

class App {
    constructor() {
        this.server = express();
        this.server.use(bodyParser.json());
        this.server.use(bodyParser.urlencoded({ extended: false }));
        this.server.use(cors());

        this.exceptionHandler();
        this.routes();
        this.definePublicPath();
    }

    routes() {
        this.server.use(routes);
    }

    exceptionHandler() {
        this.server.use(async (err, req, res, next) => {
            if (process.env.NODE_ENV == 'development') {
                return res.status(500).json(err);
            }

            return res.status(500).json({ error: 'Internal server error.' });
        });
    }

    definePublicPath() {
        this.server.use(express.static('public'));
    }
}

export default new App().server;