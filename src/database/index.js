import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import databaseJson from '../config/database';

const basename = path.basename(__filename);
const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development';

const config = databaseJson[env];

const db = {};
let sequelize;
if (env === 'production') {
    sequelize = new Sequelize(
        process.env[config.use_env_variable], config
    );
} else {
    sequelize = new Sequelize(
        config.database, config.username, config.password, config
    );
}

fs
.readdirSync(`${__dirname}/../app/models`)
.filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
})
.forEach(function (file) {
    var model = require(path.join(`${__dirname}/../app/models`, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
});

Object.keys(db).forEach(function (modelName) {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;