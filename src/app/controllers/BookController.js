import ApiBase from '../../lib/ApiBase';
import BookService from "../services/BookService";

const api = new ApiBase;
const service = new BookService;

class BookController {

    async getAllBooks(req, resp) {
        const allBooks = await service.getAll();
        api.setSuccess(200, 'Data retrieved', allBooks);
        return api.send(resp);
    }

    async getBookById(req, resp) {
        const book = await service.getById(req.params.bookId);
        api.setSuccess(200, 'Data found', book);

        if (book === null) {
            api.setError(404, 'Data not found!');
        }

        return api.send(resp);
    }

    async createBook(req, resp) {
        const { title, price, description } = req.body;
        if (!title || !price || !description) {
            api.setError(400, 'Please provide full data to create a book');
            return api.send(resp);
        }

        const newBook = req.body;
        let result = await service.store(newBook);

        api.setSuccess(201, 'Book created!');
        return api.send(resp);
    }

    async updateBook(req, resp) {
        const { title, price, description } = req.body;
        if (!title || !price || !description) {
            api.setError(400, 'Please provide full data to create a book');
            return api.send(resp);
        }

        const { bookId } = req.params;
        const upBook = req.body;

        let result = await service.update(bookId, upBook);
        api.setSuccess(200, 'Book updated!');

        if (result === null) {
            api.setError(404, 'Book not found!');
        }

        return api.send(resp);
    }

    async deleteBook(req, resp) {
        const { bookId } = req.params;
        let result = await service.delete(bookId);
        api.setSuccess(200, 'Book deleted!');

        if (result === null) {
            api.setError(404, 'Book not found!');
        }

        return api.send(resp);
    }

}

export default new BookController();