import { Router } from 'express';
import BookController from "./app/controllers/BookController";

const routes = new Router();

routes.get('/api/v1/books', BookController.getAllBooks);
routes.get('/api/v1/books/:bookId', BookController.getBookById);
routes.post('/api/v1/books', BookController.createBook);
routes.put('/api/v1/books/:bookId', BookController.updateBook);
routes.delete('/api/v1/books/:bookId', BookController.deleteBook);

export default routes;