export default class ApiBase {
    constructor() {
        this.statusCode = null;
        this.type = null;
        this.data = null;
        this.message = null;
    }

    setSuccess(statusCode, message, data) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
        this.type = 'success';
    }

    setError(statusCode, message, data = []) {
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
        this.type = 'error';

        this.errorResponse = {
            'error':{
                'status': this.type,
                'message': this.message,
                'statusCode': this.statusCode,
            }
        }

        if (this.data.length > 0) {
            this.errorResponse.error.data = this.data;
        }
    }

    send(res) {
        const result = {
            status: this.type,
            message: this.message,
            data: this.data,
        };

        if (this.type === 'success') {
            return res.status(this.statusCode).json(result);
        }

        return res.status(this.statusCode).json(this.errorResponse);
    }
}