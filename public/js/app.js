moment.locale('pt-BR');

var env = {};

// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}

var app = angular.module("LibaryOnline", [
    "ngRoute",
    "toastr",
    "angularUtils.directives.dirPagination"
]);

app.config(["paginationTemplateProvider", function(paginationTemplateProvider) {
    paginationTemplateProvider.setPath('tpl/pagination.html');
}]);

app.config(["$routeProvider", function($routeProvider) {
    $routeProvider
    .when("/", {
        title: "Book",
        controller: "bookCtrl",
        templateUrl : "views/book.html"
    })
    .when("/book", {
        title: "Book",
        controller: "bookCtrl",
        templateUrl: "views/book.html"
    })
    .otherwise({
		redirectTo: '/404'
	});
}]);


//Definindo vriaveis de ambiente
app.constant('__env', env);

app.run(['$rootScope', 'promiseApi', function ($rootScope, promiseApi) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });

    function getUserLocation() {
        promiseApi.getIp().then(function sucessCallback(response) {
            $rootScope.public_client_ip  = response.data.ip;
        });
    }
    getUserLocation();

}]);

app.config(["toastrConfig", function(toastrConfig) {
    angular.extend(toastrConfig, {
        timeOut: 3000,
        closeButton: true,
        progressBar: true,
        autoDismiss: true,
        containerId: 'toast-container',
        iconClasses: {
            error: 'toast-error',
            info: 'toast-info',
            success: 'toast-success',
            warning: 'toast-warning'
        },
        maxOpened: 3,
        newestOnTop: true,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: true,
        target: 'body'
    });
}]);

app.filter('cutText', function(){
    return function(_msg, length = 80){
        if(_msg.length > length){
            return _msg.slice(0, length) + "...";
        }else{
            return _msg;
        }
    };
});

app.filter('convertHuman', function(){
    return function(_data, format = 'YYYY-MM-DD HH:II:SS'){
        return moment(_data, format).fromNow()
    };
});