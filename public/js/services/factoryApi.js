app.factory("promiseApi", ["$http", "__env", function ($http, __env){

    let _get = function(url, params) {
        return $http({
            method: 'GET',
            url: `${__env.apiUrl}${url}`,
            params: params
        });
    };

    let _post = function(url, params) {
        return $http({
            method: 'POST',
            url: `${__env.apiUrl}${url}`,
            data: params
        });
    };

    let _put = function(url, params) {
        return $http({
            method: 'PUT',
            url: `${__env.apiUrl}${url}`,
            data: params
        });
    };

    let _delete = function(url, params) {
        return $http({
            method: 'DELETE',
            url: `${__env.apiUrl}${url}`,
            params: params
        });
    };

    let _getIp = function() {
        return $http({
            method: 'GET',
            url: 'https://jsonip.com/'
        });
    };

    return {
        get:_get,
        post:_post,
        put:_put,
        delete:_delete,
        getIp:_getIp
    };
}]);