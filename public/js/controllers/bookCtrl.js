app.controller("bookCtrl", ["$scope", "promiseApi", "toastr", function($scope, promiseApi, toastr) {
    $scope.table_control = '10';
    $scope.error_lock_screen = false;

    $scope.getData = function() {
        $scope.page_title = "Gestão de Livros";
        $scope.form_title = "Cadastrar novo livro";
        $scope.btn_title = "Cadastrar";

        $scope.listBooks();
    };

    $scope.listBooks = function() {
        promiseApi.get('books', {}).then(function sucessCallback(response) {
            $scope.list_book = response.data.data;
        }, function errorCallback(response) {
            console.error(response.data);
            $scope.error_lock_screen = "Houve um erro ao buscar listagem de livros... Desculpe o transtorno =( ";
            toastr.error('Erro ao buscar a lista de livros');
        });
    }

    $scope.instUpdData = function(params) {
        let method = 'post';
        let route = 'books';

        if (params.id > 0) {
            method = 'put';
            route = `${route}/${params.id}`;
        }

        promiseApi[method](route, params).then(function sucessCallback(response) {
            let { status } = response;
            let msg_toast = 'Livro criado com sucesso!';
            if (status == 200) {
                msg_toast = 'Livro atualizado com sucesso!'
            }

            toastr.success(msg_toast);
            delete $scope.params;
            $scope.getData();
        }, function errorCallback(response) {
            console.log(response);
            toastr.error('Ocorreu um erro inesperado');
        })
    };

    $scope.updateData = function(params) {
        $scope.params = {...params};
        $scope.btn_title = "Atualizar";
        $scope.form_title = "Atualizar Livro";
    };

    $scope.newData = function() {
        $scope.btn_title = "Cadastrar";
        $scope.form_title = "Cadastrar novo livro";
    };

    $scope.deleteData = function(data) {
        Swal.fire({
            title: "Deseja apagar o livro?",
            html: `Deseja realmente apagar o livro: <strong>${data.title}</strong> (é uma ação irreversível)`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim, apague!'
        }).then((result) => {
            if (result.value) {
                promiseApi.delete(`books/${data.id}`, {}).then(function sucessCallback(response) {
                    toastr.success('Livro apagado com sucesso!');
                    delete $scope.params;
                    $scope.getData();
                }, function errorCallback(response) {
                    toastr.error('Erro ao apagar o livro');
                });
            }
        })
    };

}]);