"use strict";

module.exports = function (sequelize, DataTypes) {
  var Book = sequelize.define('Book', {
    title: DataTypes.STRING,
    price: DataTypes.DOUBLE,
    description: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    tableName: 'books'
  });
  return Book;
};
//# sourceMappingURL=Book.js.map