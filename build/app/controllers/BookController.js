"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _ApiBase = _interopRequireDefault(require("../../lib/ApiBase"));

var _BookService = _interopRequireDefault(require("../services/BookService"));

var api = new _ApiBase["default"]();
var service = new _BookService["default"]();

var BookController = /*#__PURE__*/function () {
  function BookController() {
    (0, _classCallCheck2["default"])(this, BookController);
  }

  (0, _createClass2["default"])(BookController, [{
    key: "getAllBooks",
    value: function () {
      var _getAllBooks = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, resp) {
        var allBooks;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return service.getAll();

              case 2:
                allBooks = _context.sent;
                api.setSuccess(200, 'Data retrieved', allBooks);
                return _context.abrupt("return", api.send(resp));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getAllBooks(_x, _x2) {
        return _getAllBooks.apply(this, arguments);
      }

      return getAllBooks;
    }()
  }, {
    key: "getBookById",
    value: function () {
      var _getBookById = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, resp) {
        var book;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return service.getById(req.params.bookId);

              case 2:
                book = _context2.sent;
                api.setSuccess(200, 'Data found', book);

                if (book === null) {
                  api.setError(404, 'Data not found!');
                }

                return _context2.abrupt("return", api.send(resp));

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getBookById(_x3, _x4) {
        return _getBookById.apply(this, arguments);
      }

      return getBookById;
    }()
  }, {
    key: "createBook",
    value: function () {
      var _createBook = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, resp) {
        var _req$body, title, price, description, newBook, result;

        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _req$body = req.body, title = _req$body.title, price = _req$body.price, description = _req$body.description;

                if (!(!title || !price || !description)) {
                  _context3.next = 4;
                  break;
                }

                api.setError(400, 'Please provide full data to create a book');
                return _context3.abrupt("return", api.send(resp));

              case 4:
                newBook = req.body;
                _context3.next = 7;
                return service.store(newBook);

              case 7:
                result = _context3.sent;
                api.setSuccess(201, 'Book created!');
                return _context3.abrupt("return", api.send(resp));

              case 10:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function createBook(_x5, _x6) {
        return _createBook.apply(this, arguments);
      }

      return createBook;
    }()
  }, {
    key: "updateBook",
    value: function () {
      var _updateBook = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, resp) {
        var _req$body2, title, price, description, bookId, upBook, result;

        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _req$body2 = req.body, title = _req$body2.title, price = _req$body2.price, description = _req$body2.description;

                if (!(!title || !price || !description)) {
                  _context4.next = 4;
                  break;
                }

                api.setError(400, 'Please provide full data to create a book');
                return _context4.abrupt("return", api.send(resp));

              case 4:
                bookId = req.params.bookId;
                upBook = req.body;
                _context4.next = 8;
                return service.update(bookId, upBook);

              case 8:
                result = _context4.sent;
                api.setSuccess(200, 'Book updated!');

                if (result === null) {
                  api.setError(404, 'Book not found!');
                }

                return _context4.abrupt("return", api.send(resp));

              case 12:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function updateBook(_x7, _x8) {
        return _updateBook.apply(this, arguments);
      }

      return updateBook;
    }()
  }, {
    key: "deleteBook",
    value: function () {
      var _deleteBook = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, resp) {
        var bookId, result;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                bookId = req.params.bookId;
                _context5.next = 3;
                return service["delete"](bookId);

              case 3:
                result = _context5.sent;
                api.setSuccess(200, 'Book deleted!');

                if (result === null) {
                  api.setError(404, 'Book not found!');
                }

                return _context5.abrupt("return", api.send(resp));

              case 7:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function deleteBook(_x9, _x10) {
        return _deleteBook.apply(this, arguments);
      }

      return deleteBook;
    }()
  }]);
  return BookController;
}();

var _default = new BookController();

exports["default"] = _default;
//# sourceMappingURL=BookController.js.map