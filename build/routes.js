"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _BookController = _interopRequireDefault(require("./app/controllers/BookController"));

var routes = new _express.Router();
routes.get('/api/v1/books', _BookController["default"].getAllBooks);
routes.get('/api/v1/books/:bookId', _BookController["default"].getBookById);
routes.post('/api/v1/books', _BookController["default"].createBook);
routes.put('/api/v1/books/:bookId', _BookController["default"].updateBook);
routes["delete"]('/api/v1/books/:bookId', _BookController["default"].deleteBook);
var _default = routes;
exports["default"] = _default;
//# sourceMappingURL=routes.js.map