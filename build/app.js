"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _express = _interopRequireDefault(require("express"));

var _routes2 = _interopRequireDefault(require("./routes"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _cors = _interopRequireDefault(require("cors"));

var App = /*#__PURE__*/function () {
  function App() {
    (0, _classCallCheck2["default"])(this, App);
    this.server = (0, _express["default"])();
    this.server.use(_bodyParser["default"].json());
    this.server.use(_bodyParser["default"].urlencoded({
      extended: false
    }));
    this.server.use((0, _cors["default"])());
    this.exceptionHandler();
    this.routes();
  }

  (0, _createClass2["default"])(App, [{
    key: "routes",
    value: function routes() {
      this.server.use(_routes2["default"]);
    }
  }, {
    key: "exceptionHandler",
    value: function exceptionHandler() {
      this.server.use( /*#__PURE__*/function () {
        var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(err, req, res, next) {
          return _regenerator["default"].wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  if (!(process.env.NODE_ENV == 'development')) {
                    _context.next = 2;
                    break;
                  }

                  return _context.abrupt("return", res.status(500).json(err));

                case 2:
                  return _context.abrupt("return", res.status(500).json({
                    error: 'Internal server error.'
                  }));

                case 3:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function (_x, _x2, _x3, _x4) {
          return _ref.apply(this, arguments);
        };
      }());
    }
  }]);
  return App;
}();

var _default = new App().server;
exports["default"] = _default;
//# sourceMappingURL=app.js.map