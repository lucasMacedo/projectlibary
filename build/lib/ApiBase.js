"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var ApiBase = /*#__PURE__*/function () {
  function ApiBase() {
    (0, _classCallCheck2["default"])(this, ApiBase);
    this.statusCode = null;
    this.type = null;
    this.data = null;
    this.message = null;
  }

  (0, _createClass2["default"])(ApiBase, [{
    key: "setSuccess",
    value: function setSuccess(statusCode, message, data) {
      this.statusCode = statusCode;
      this.message = message;
      this.data = data;
      this.type = 'success';
    }
  }, {
    key: "setError",
    value: function setError(statusCode, message) {
      var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
      this.statusCode = statusCode;
      this.message = message;
      this.data = data;
      this.type = 'error';
      this.errorResponse = {
        'error': {
          'status': this.type,
          'message': this.message,
          'statusCode': this.statusCode
        }
      };

      if (this.data.length > 0) {
        this.errorResponse.error.data = this.data;
      }
    }
  }, {
    key: "send",
    value: function send(res) {
      var result = {
        status: this.type,
        message: this.message,
        data: this.data
      };

      if (this.type === 'success') {
        return res.status(this.statusCode).json(result);
      }

      return res.status(this.statusCode).json(this.errorResponse);
    }
  }]);
  return ApiBase;
}();

exports["default"] = ApiBase;
//# sourceMappingURL=ApiBase.js.map